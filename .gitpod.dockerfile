FROM gitpod/workspace-full

RUN sudo apt-get update && \
    sudo apt-get install gettext -y  && \
    sudo apt-get install -y sshpass && \
    brew install derailed/k9s/k9s && \
    brew install helm && \
    brew install kubernetes-cli helm && \
    brew install kompose

USER gitpod

RUN brew tap civo/tools && \
    brew install civo 

#RUN brew install exa && \
#    brew install bat && \
#    brew install httpie && \
#    brew install hey



