#!/bin/bash
set -o allexport; source .env; set +o allexport

echo "🥇[4/4]  get the PEM certificate (./config/certificate.txt)"

echo $(kubectl get secret -o go-template='{{index .data "ca.crt" }}' $(kubectl get sa default -o go-template="{{range .secrets}}{{.name}}{{end}}")) | base64 -d > ./config/certificate.txt

cat ./config/certificate.txt

